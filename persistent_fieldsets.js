/* $Id$ */



(function ($) {
  
  /**
 * Send the details of all the fieldsets when the user navigates away from the page
 */
  $(window).unload(function(e){
    //Call the update function when the user navigates away from the page
    Drupal.sendFieldsets();

  });


  /**
   * Function to send details about all fieldset
   */
  Drupal.sendFieldsets = function(){

    //array to save fieldset details
    var fieldsetStats = [];
    //get all the fieldsets
    var fieldsets = $('.persistent-fieldset');
    var collapsed
    //Get details for each fieldset
    $(fieldsets).each(function(i, fieldset){
      //console.log(($(fieldset).hasClass('collapsed')) ? 1 : 0 )
      //fieldsetStats +=  $(fieldset).attr('id') + "=" +  (($(fieldset).hasClass('collapsed')) ? 1 : 0)  + "&";

      fieldsetStats.push({ 
        name :  $(fieldset).attr('id'),
        state: ($(fieldset).hasClass('collapsed') ? 1 : 0)
      });
    });

    //console.log(fieldsetStats);
    
    $.ajax({
      type : 'POST',
      url : Drupal.settings.basePath + 'pf/save',
      async: false,
      dataType: 'json',
      data: {
        data : JSON.stringify(fieldsetStats)
      },
      success : function(data){
        //console.info(data);
      },
      error: function(xhr, error){
        console.info('Error');
        //console.log(error);
        //console.log(xhr);
      }
    });
  }

})(jQuery);





